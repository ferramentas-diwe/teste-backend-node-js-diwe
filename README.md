# Teste para desenvolvedor back-end Node.js

Desenvolver uma RESTful API a partir do estudo de caso proposto

## Premissas
- Utilizar banco de dados relacional
- Utilizar algum framework do Node.js
- Modelagem do banco de dados com o desafio proposto

## Diferenciais
- Documentação dos endpoints da API
- Utilizar o framework Loopback
- Testes
- Documentação do projeto em geral

## Objetivo
O objetivo do desafio é construir uma API a partir do modelo proposto, queremos analisar suas capacidades em planejamento e modelagem de tabelas.

## O Desafio
João tem em seu sistema uma tabela com as roles do seu sistema (ANEXO 1) e uma tabela de categorias (ANEXO 2), porém elas ainda não estão em uso. 

João precisa listar todas as roles e suas respectivas categorias. Ele definiu que como regra de negócio que uma role pode ter várias categorias e cada categoria cadastrada pode pertencer a mais de uma role.

João também deseja gerenciar (CREATE, READ, UPDATE, DELETE) as categorias e roles do seu sistema.

## Pontos de atenção

Vamos analisar sua capacidade de interpretação da situação problema, como vai se comportar e propor soluções.

## O que vamos avaliar?
1. Funcionamento e método de resolução do problema.
2. Organização do código.
3. Performance do código.
4. Documentação da API.
5. Arquitetura do projeto.
6. Semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões.
7. Históricos de commits do git.

## Entrega

Você deve enviar o link do repositório PUBLICO para o endereço de e-mail: 

mateus.silva@diwe.com.br CC: maicon.passos@diwe.com.br

# ANEXO 1 - Tabela de Roles

| id |  RoleName  |  Description  |  Active  |
|--- | ---------- | ---------- | ---------- |
| 1  |  Jogador | Jogador de futebol | true
| 2  |  Colecionador | Colecionador de álbums | true
| 3  |  Cliente | Cliente da banca | false

# ANEXO 2 - Tabela de Categorias

| id |  CategoryName | Active  |
|--- | ---------- | ---------- |
| 1  |  Copa do Mundo | true
| 2  |  Super Herói | true
| 3  |  Futebol | false


